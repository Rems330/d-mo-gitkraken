--******************************************************************************
--Projet : Outils de Billing RPE
--Prestataire : Sopra Group
--Programme: rpe_fac_s_extract_rubr_new_pkb.sql
--$LastChangedRevision: 30027 $
--******************************************************************************

SET VERIFY OFF
SET DEFINE OFF
WHENEVER SQLERROR EXIT FAILURE ROLLBACK;
WHENEVER OSERROR EXIT FAILURE ROLLBACK;  

PROMPT ================================================
PROMPT CREATION PACKAGE BODY RPE_FAC_S_EXTRACT_RUBR_NEW
PROMPT ================================================    

 
       
create or replace PACKAGE BODY RPE_FAC_S_EXTRACT_RUBR_NEW AS

    l_fic_sortie UTL_FILE.file_type;
    l_file_name  VARCHAR2(50) := 'debut';
    l_nom_fichier  VARCHAR2(50);
    l_enrgt VARCHAR2(500);
    l_debut_fac NUMBER(2):= 0;
    l_genre NUMBER(3):= 0;
    l_bill_ref_no NUMBER(10):= 0;
    l_bill_ref_resets NUMBER(3) := 0;
    l_fac_existe NUMBER(1):= 0;
    l_genre_special NUMBER(1);
	l_libelle VARCHAR2(100);

    ------------------------------------------------------------------------------------
    --    Sous-fonction : Traitement partie corp
    ------------------------------------------------------------------------------------ 
    function ecrire_TVA(v_genre IN NUMBER, v_pxHT IN NUMBER,v_PXTVA IN NUMBER,l_fic_sortie UTL_FILE.file_type)
    RETURN NUMBER 
    IS
        BEGIN
            BEGIN
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Ouverture de la fonction ecrire TVA',RPE_RPE_COM_C_LOG.g_info);
            UTL_FILE.put_line(l_fic_sortie, '<TVA genre="'||v_genre||'">');
            UTL_FILE.put_line(l_fic_sortie, '<PXHT>'||v_pxHT||'</PXHT>');
            UTL_FILE.put_line(l_fic_sortie, '<PXTVA>'||v_pxTVA||'</PXTVA>');
            UTL_FILE.put_line(l_fic_sortie, '</TVA>');
        EXCEPTION
            when others then
                return RPE_RPE_C_COMMUN.g_echec;
            END;
        RETURN RPE_RPE_C_COMMUN.g_succes;
    END ecrire_TVA;
        
        

    function partie_corp(bill_ref_no IN NUMBER,
                         bill_ref_resets IN NUMBER
                        )
    RETURN NUMBER   
    IS    
        -- Curseur traitement partie corp
        CURSOR c_corp (v_bill_ref_no IN number,
                       v_bill_ref_resets IN number)
        IS
          select distinct
                 rfhf.bill_ref_no,
                 rfhf.bill_ref_resets,
                 rfhf.refcli,
                 rfhf.account_no,
                 rfhf.catcli,
                 rfhf.vipcode,
                 rfhf.modepaie,
                 cmf.billing_frequency, 
                 cmf.bill_name_pre,     
                 cmf.bill_fname,
                 cmf.bill_lname,
                 cmf.bill_company,
                 cmf.bill_address3, --<ADR1>
                 cmf.bill_address2, --<ADR2>
                 cmf.bill_address1, --<ADR3>
                 cmf.bill_zip,
                 cmf.bill_city,
                 cmf.cust_email,
                 rfhf.date_creation,
                 rfhf.date_fin_eng_abo,
                 rfhf.date_fin_eng_tv,
                 rfhf.date_fin_eng_rmc,
                 rfhf.date_fin_eng_plus,
                 rfhf.liste_ndi_ndi_type,
                 rfhf.statement_date,
                 rfhf.from_date,
                 rfhf.to_date,
                 rfhf.demat,
                 rfhf.demdetail,
                 rfhf.format_facture,
                 rfhf.special_code,
                 rfhf.typegen,
                 rfhf.braille,
                 rfhf.agrandis,
                 rfhf.achat,
                 rfhf.origine,
                 rfhf.id_origine,
                 rfhf.nb_fact_btb,
                 rfhf.nb_fact_mens_btb,
                 rfhf.offre_principale,
                 rfhf.statut_joya_val,
                 rfhf.statut_joya_lib,
                 rfhf.rcs,
                 rfhf.client_type,
                 pp.cust_bank_sort_code,
                 pp.bank_agency_code,
                 pp.cust_bank_acc_owner,
                 pp.cust_bank_acc_num,
                 rfhf.cle_rib,
                 pp.bank_agency_name,
                 rfhf.rum,
                 rfhf.iban,
                 rfhf.code_barre,
                 rfhf.TOTFACSFRHT,
                 rfhf.TOTFACSFRTTC,
                 rfhf.TOTFACFCTHT,
                 rfhf.TOTFACFCTTTC,
                 rfhf.totht,
                 rfhf.tottva,
                 rfhf.totttc,
                 rfhf.TOTFACPCT,
                 rfhf.MNT_PREL_DEPOT_TTC,
                 rfhf.MNT_DEJAPREL_DEPOT_TTC,
                 rfhf.SOLDPREC,
                 rfhf.PAI,
                 rfhf.REMBT,
                 rfhf.RETDEPOT,
                 rfhf.SOLDACT,
                 rfhf.totpaiettc,
                 rfhf.payment_due_date,
                 rfhf.CODEEMET,
                 rfhf.CODEETAB,
                 rfhf.CLE3,
                 rfhf.CLE2,
                 rfhf.REFOP,
                 rfhf.CDOC,
                 rfhf.CLE1,
                 rfhf.CNAT,
                 rfhf.CCB
          from RPE_FAC_S_HEAD_FACT_NEW rfhf
          inner join RPE_FAC_S_DET_FACT_NEW rfdf on rfdf.bill_ref_no = rfhf.bill_ref_no
                                                 and rfdf.bill_ref_resets = rfhf.bill_ref_resets
          inner join CMF cmf on cmf.account_no = rfhf.account_no
          inner join PAYMENT_PROFILE pp on pp.account_no = rfhf.account_no
          where rfdf.bill_ref_no = v_bill_ref_no
          and rfdf.bill_ref_resets = v_bill_ref_resets
          and rfhf.bill_ref_no = v_bill_ref_no
          and rfhf.bill_ref_resets = v_bill_ref_resets
          and rfhf.account_no = cmf.account_no
          and cmf.account_no = pp.account_no;
          
          --curseur pour la partie <RCDISCOUNT>
          CURSOR c_component_id (v_bill_ref_no IN number,
                                 v_bill_ref_resets IN number
                                 )
          IS
            SELECT DISTINCT rfdf.component_id
            from RPE_FAC_S_DET_FACT_NEW rfdf
            where rfdf.bill_ref_no = v_bill_ref_no
            and rfdf.bill_ref_resets = v_bill_ref_resets
            and (type_charge = 2
                   and product_line_id != -1
                   and substr(component_id,3,1) != 8
                   )
                   or type_charge = 50
                   or type_charge = 54;

            v_first_tva boolean := TRUE;
            v_genre number(3);
            
            --curseur pour la TVA pour les genre de 1 a 5
            cursor c_tva15(v_bill_ref_no IN number,
                    v_bill_ref_resets IN number)
            is
            select 
            CASE 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=196000
                            THEN 1  
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=55000
                            THEN 2 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=85000
                            THEN 3 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=21000 and cmf.account_category=5
                            THEN 4    
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=70000
                            THEN 5    
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=10500 and cmf.account_category=5
                            THEN 10
                            ELSE null
            END  as genre,
            sum(bid.tax) as tax,
            sum(bid.amount)  as amount
            from bill_invoice_detail bid
            inner join CMF_balance cb
            on cb.bill_ref_no=bid.bill_ref_no
            and cb.bill_ref_resets=bid.bill_ref_resets
            inner join cmf 
            on cmf.account_no=cb.account_no
            where bid.bill_ref_no=v_bill_ref_no
            and bid.bill_ref_resets=v_bill_ref_resets
            and bid.type_code=5
            and bid.subtype_code=-7
            and bid.tax_rate in (196000,55000,85000,21000,70000,10500)
            group by CASE 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=196000
                            THEN 1  
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=55000
                            THEN 2 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=85000
                            THEN 3 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=21000 and cmf.account_category=5
                            THEN 4    
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=70000
                            THEN 5    
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=10500 and cmf.account_category=5
                            THEN 10
                            ELSE null
                      END
            order by genre;
            
            -- curseur pour TVA ou genre = 6
            cursor c_tva6 (v_bill_ref_no IN number,
                    v_bill_ref_resets IN number)
            is select
            6 as genre, sum(bid.tax) as tax,sum(bid.amount)as amount
            from bill_invoice_detail bid
            where bid.bill_ref_no=v_bill_ref_no
            and bid.bill_ref_resets=v_bill_ref_resets
            and bid.type_code in (2,3,7) 
            and nvl(bid.product_line_id,0) not in (105,106,29,30) 
            and bid.subtype_code <> 622 
            and bid.tax_rate = 0;
            
            --curseur pour la TVA pour les genre de 7 a 10
            cursor c_tva710(v_bill_ref_no IN number,
                    v_bill_ref_resets IN number)
            is
            select 
            CASE 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=200000
                            THEN 7
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=100000
                            THEN 8 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=21000 and cmf.account_category<>5
                            THEN 9
                            ELSE null
                        END  as genre,
            sum(bid.tax) as tax,
            sum(bid.amount) as amount
            from bill_invoice_detail bid
            inner join CMF_balance cb
            on cb.bill_ref_no=bid.bill_ref_no
            and cb.bill_ref_resets=bid.bill_ref_resets
            inner join cmf 
            on cmf.account_no=cb.account_no
            where bid.bill_ref_no=v_bill_ref_no
            and bid.bill_ref_resets=v_bill_ref_resets
            and bid.type_code=5
            and bid.subtype_code=-7
            and bid.tax_rate in (200000,100000,21000)
            group by CASE 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=200000
                            THEN 7
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=100000
                            THEN 8 
                            WHEN bid.type_code=5 and bid.subtype_code=-7 and bid.tax_rate=21000 and cmf.account_category<>5
                            THEN 9
                            ELSE null
                        END
            order by genre;
            
            
            
        BEGIN
         RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_corp',RPE_RPE_COM_C_LOG.g_info); 
          BEGIN
            FOR c_r_corp IN c_corp (bill_ref_no, bill_ref_resets) LOOP
                    UTL_FILE.put_line(l_fic_sortie, '<CORP>');
                    UTL_FILE.put_line(l_fic_sortie, '<INFCLI><NUM>' || c_r_corp.bill_ref_no || '</NUM>');
                    UTL_FILE.put_line(l_fic_sortie, '<REFCLI>' || c_r_corp.refcli || '</REFCLI>');
                    UTL_FILE.put_line(l_fic_sortie, '<NOCPTE>' || c_r_corp.account_no || '</NOCPTE>');
                    UTL_FILE.put_line(l_fic_sortie, '<CATCLI>' || c_r_corp.catcli || '</CATCLI>');
                    UTL_FILE.put_line(l_fic_sortie, '<VIPCODE>' || c_r_corp.vipcode || '</VIPCODE>');
                    UTL_FILE.put_line(l_fic_sortie, '<MODEPAIE>' || c_r_corp.modepaie || '</MODEPAIE>');
                    UTL_FILE.put_line(l_fic_sortie, '<FREQFACT>' || c_r_corp.billing_frequency || '</FREQFACT>');
                    UTL_FILE.put_line(l_fic_sortie, '<CIVCLI>' || c_r_corp.bill_name_pre || '</CIVCLI>');
                    UTL_FILE.put_line(l_fic_sortie, '<PRECLI>' || c_r_corp.bill_fname || '</PRECLI>');
                    UTL_FILE.put_line(l_fic_sortie, '<NOMCLI>' || c_r_corp.bill_lname || '</NOMCLI>');
                    IF (c_r_corp.bill_company is not null)then
                        UTL_FILE.put_line(l_fic_sortie, '<NOMSOC>' || c_r_corp.bill_company || '</NOMSOC>');
                    END IF;
                    IF (c_r_corp.bill_address3 is not null) then
                        UTL_FILE.put_line(l_fic_sortie, '<ADR1>' || c_r_corp.bill_address3 || '</ADR1>');
                    END IF;
                    UTL_FILE.put_line(l_fic_sortie, '<ADR2>' || c_r_corp.bill_address2 || '</ADR2>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR3>' || c_r_corp.bill_address1 || '</ADR3>');
                    UTL_FILE.put_line(l_fic_sortie, '<CP>' || c_r_corp.bill_zip || '</CP>');
                    UTL_FILE.put_line(l_fic_sortie, '<VILLE>' || c_r_corp.bill_city || '</VILLE>');
                    UTL_FILE.put_line(l_fic_sortie, '<MAIL>' || c_r_corp.cust_email || '</MAIL>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATECREA>' || to_char(c_r_corp.date_creation,'DDMMYYYY')|| '</DATECREA>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATE_FIN_ENG_ABO>' || to_char(c_r_corp.date_fin_eng_abo,'DDMMYYYY') || '</DATE_FIN_ENG_ABO>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATE_FIN_ENG_TV>' || to_char(c_r_corp.date_fin_eng_tv,'DDMMYYYY')|| '</DATE_FIN_ENG_TV>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATE_FIN_RMC>' ||  to_char(c_r_corp.date_fin_eng_rmc,'DDMMYYYY')|| '</DATE_FIN_RMC>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATE_FIN_PLUS>' || to_char(c_r_corp.date_fin_eng_plus,'DDMMYYYY')|| '</DATE_FIN_PLUS>');
                    UTL_FILE.put_line(l_fic_sortie, '</INFCLI><RCDISCOUNT>');
                    FOR components in c_component_id (bill_ref_no, bill_ref_resets) LOOP
                        UTL_FILE.put_line(l_fic_sortie, '<ID_ARBOR>' || components.COMPONENT_ID || '</ID_ARBOR>');
                    END LOOP;
                    UTL_FILE.put_line(l_fic_sortie, '</RCDISCOUNT>');
                    UTL_FILE.put_line(l_fic_sortie, '<CLILIGNES>');
                    UTL_FILE.put_line(l_fic_sortie,c_r_corp.liste_ndi_ndi_type);
                    UTL_FILE.put_line(l_fic_sortie,'</CLILIGNES>'); 
                    UTL_FILE.put_line(l_fic_sortie, '<INFFAC><NUM>' || c_r_corp.bill_ref_no || '</NUM>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATE>' || to_char(c_r_corp.statement_date,'DDMMYYYY') || '</DATE>');
                    UTL_FILE.put_line(l_fic_sortie, '<FROM>' || to_char(c_r_corp.from_date,'DDMMYYYY') || '</FROM>');
                    UTL_FILE.put_line(l_fic_sortie, '<TO>' || to_char(c_r_corp.to_date,'DDMMYYYY')|| '</TO>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATEPREC>' || to_char(c_r_corp.statement_date,'DDMMYYYY')|| '</DATEPREC>');
                    UTL_FILE.put_line(l_fic_sortie, '<DEMAT>' || c_r_corp.demat || '</DEMAT>');
                    UTL_FILE.put_line(l_fic_sortie, '<DEMDETAIL>' || c_r_corp.demdetail || '</DEMDETAIL>');
                    UTL_FILE.put_line(l_fic_sortie, '<FORMFACT>' || c_r_corp.format_facture || '</FORMFACT>');
                    UTL_FILE.put_line(l_fic_sortie, '<CIRC>' || c_r_corp.special_code || '</CIRC>');
                    UTL_FILE.put_line(l_fic_sortie, '<TYPEGEN>' || c_r_corp.typegen || '</TYPEGEN>');
                    UTL_FILE.put_line(l_fic_sortie, '<BRAILLE>' || c_r_corp.braille || '</BRAILLE>');
                    UTL_FILE.put_line(l_fic_sortie, '<AGRANDIS>' || c_r_corp.agrandis || '</AGRANDIS>');
                    UTL_FILE.put_line(l_fic_sortie, '<ACHAT>' || c_r_corp.achat || '</ACHAT>');
                    UTL_FILE.put_line(l_fic_sortie, '<ORIGINE>' || c_r_corp.origine || '</ORIGINE>');
                    UTL_FILE.put_line(l_fic_sortie, '<ID_ORIGINE>' || c_r_corp.id_origine || '</ID_ORIGINE>');
                    UTL_FILE.put_line(l_fic_sortie, '<NB_FACT_BTB>' || c_r_corp.nb_fact_btb || '</NB_FACT_BTB>');
                    UTL_FILE.put_line(l_fic_sortie, '<NB_FACT_MENS_BTB>' || c_r_corp.nb_fact_mens_btb || '</NB_FACT_MENS_BTB>');
                    UTL_FILE.put_line(l_fic_sortie, '<OFFRE_PRINCIPALE>' || c_r_corp.offre_principale || '</OFFRE_PRINCIPALE>');
                    UTL_FILE.put_line(l_fic_sortie, '<STATUT_JOYA_VAL>' || c_r_corp.statut_joya_val || '</STATUT_JOYA_VAL>');
                    UTL_FILE.put_line(l_fic_sortie, '<STATUT_JOYA_LIB>' || c_r_corp.statut_joya_lib || '</STATUT_JOYA_LIB>');
                    UTL_FILE.put_line(l_fic_sortie, '<RCS>' || c_r_corp.rcs || '</RCS>');
                    IF c_r_corp.billing_frequency = 0 then
                       UTL_FILE.put_line(l_fic_sortie, '<CLIENT_TYPE>' || c_r_corp.client_type || '</CLIENT_TYPE>');
                    ELSE NULL;
                    END IF;
                    UTL_FILE.put_line(l_fic_sortie, '</INFFAC>');
                    UTL_FILE.put_line(l_fic_sortie, '<INFBQE><NOM>' || c_r_corp.cust_bank_acc_owner || '</NOM>'); --verifier que c'est le bon champs choisis en prod
                    UTL_FILE.put_line(l_fic_sortie, '<CODE>' || c_r_corp.cust_bank_sort_code || '</CODE>');
                    UTL_FILE.put_line(l_fic_sortie, '<CODEAGENCE>' || c_r_corp.bank_agency_code || '</CODEAGENCE>');
                    UTL_FILE.put_line(l_fic_sortie, '<NOMTITUCPTE>' || c_r_corp.cust_bank_acc_owner || '</NOMTITUCPTE>');
                    UTL_FILE.put_line(l_fic_sortie, '<NOCPTEBQ>' || c_r_corp.cust_bank_acc_num || '</NOCPTEBQ>');
                    UTL_FILE.put_line(l_fic_sortie, '<CLE>' || c_r_corp.cle_rib || '</CLE>');
                    UTL_FILE.put_line(l_fic_sortie, '<IBAN>' || substr(c_r_corp.bank_agency_name,0,27) || '</IBAN>');
                    UTL_FILE.put_line(l_fic_sortie, '<BIC>' || substr(c_r_corp.bank_agency_name,27) || '</BIC>');
                    UTL_FILE.put_line(l_fic_sortie, '</INFBQE>');
                    UTL_FILE.put_line(l_fic_sortie, '<INFTIP><RUM>' || c_r_corp.rum || '</RUM>');
                    UTL_FILE.put_line(l_fic_sortie, '<IBAN>' || c_r_corp.iban || '</IBAN>');
                    UTL_FILE.put_line(l_fic_sortie, '<ICS>FR44ZZZ332801</ICS>');
                    UTL_FILE.put_line(l_fic_sortie, '<CODEBARRE>' || c_r_corp.code_barre || '</CODEBARRE>');
                    UTL_FILE.put_line(l_fic_sortie, '</INFTIP>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTFAC><TOTFACSFRHT>' || c_r_corp.TOTFACSFRHT || '</TOTFACSFRHT>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTFACSFRTTC>' || c_r_corp.TOTFACSFRTTC || '</TOTFACSFRTTC>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTFACFCTHT>' || c_r_corp.TOTFACFCTHT || '</TOTFACFCTHT>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTFACFCTTTC>' || c_r_corp.TOTFACFCTTTC || '</TOTFACFCTTTC>');
                  --restitution TVA
                    FOR c_t in c_tva15(bill_ref_no,bill_ref_resets) LOOP
                        IF(v_first_TVA and c_t.genre is not null) THEN
                            UTL_FILE.put_line(l_fic_sortie, '<TABTVA>');
                            v_first_TVA := FALSE;
                        END IF;
                        if(c_t.genre is not null) then
                            if(ecrire_TVA(c_t.genre, c_t.amount,c_t.tax,l_fic_sortie)=RPE_RPE_C_COMMUN.g_echec) then
                                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Echec lors de l extraction de la TVA genre 1 a 5',RPE_RPE_COM_C_LOG.g_warning);
                            END IF;
                        END IF;
                    END LOOP;
                    FOR c_t in c_tva6(bill_ref_no,bill_ref_resets) LOOP
                        IF(v_first_TVA and c_t.genre is not null) THEN
                            UTL_FILE.put_line(l_fic_sortie, '<TABTVA>');
                            v_first_TVA := FALSE;
                        END IF;
                        if(c_t.genre is not null) then
                            if(ecrire_TVA(c_t.genre, c_t.amount,c_t.tax,l_fic_sortie)=RPE_RPE_C_COMMUN.g_echec)then
                                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Echec lors de l extraction de la TVA genre 6',RPE_RPE_COM_C_LOG.g_warning);
                            END IF;
                        END IF;
                    END LOOP;
                    FOR c_t in c_tva710(bill_ref_no,bill_ref_resets) LOOP
                        IF(v_first_TVA and c_t.genre is not null) THEN
                            UTL_FILE.put_line(l_fic_sortie, '<TABTVA>');
                            v_first_TVA := FALSE;
                        END IF;
                        if(c_t.genre is not null) then
                            if(ecrire_TVA(c_t.genre, c_t.amount,c_t.tax,l_fic_sortie)=RPE_RPE_C_COMMUN.g_echec)then
                                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Echec lors de l extraction de la TVA genre 7 a 10',RPE_RPE_COM_C_LOG.g_warning);
                                END IF;
                        END IF;
                    END LOOP;  
                        if(v_first_TVA = FALSE) THEN
                             UTL_FILE.put_line(l_fic_sortie, '</TABTVA>');
                        END IF;   
                    --FIN restituion TVA
                    UTL_FILE.put_line(l_fic_sortie, '<TOTHT>' || c_r_corp.totht || '</TOTHT>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTTVA>' || c_r_corp.tottva || '</TOTTVA>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTTTC>' || c_r_corp.totttc || '</TOTTTC>');
                    UTL_FILE.put_line(l_fic_sortie, '</TOTFAC>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTFACPCT>' || c_r_corp.TOTFACPCT || '</TOTFACPCT>');
                    UTL_FILE.put_line(l_fic_sortie, '<MNT_PREL_DEPOT_TTC>' || c_r_corp.MNT_PREL_DEPOT_TTC || '</MNT_PREL_DEPOT_TTC>'); 
                    UTL_FILE.put_line(l_fic_sortie, '<MNT_DEJAPREL_DEPOT_TTC>' || c_r_corp.MNT_DEJAPREL_DEPOT_TTC || '</MNT_DEJAPREL_DEPOT_TTC>'); 
                    UTL_FILE.put_line(l_fic_sortie, '<SITU><SOLDPREC>' || c_r_corp.SOLDPREC || '</SOLDPREC>');
                    UTL_FILE.put_line(l_fic_sortie, '<PAI>' || c_r_corp.PAI || '</PAI>');
                    IF (c_r_corp.REMBT is not null) then
                    UTL_FILE.put_line(l_fic_sortie, '<REMBT>' || c_r_corp.REMBT || '</REMBT>');
                    END IF;
                    UTL_FILE.put_line(l_fic_sortie, '<RETDEPOT>' || c_r_corp.RETDEPOT || '</RETDEPOT>');
                    UTL_FILE.put_line(l_fic_sortie, '<SOLDACT>' || c_r_corp.SOLDACT || '</SOLDACT>');
                    UTL_FILE.put_line(l_fic_sortie, '</SITU>');
                    UTL_FILE.put_line(l_fic_sortie, '<TOTPAIE><DATE>' || to_char(c_r_corp.payment_due_date,'DDMMYYYY') || '</DATE>'); 
                    UTL_FILE.put_line(l_fic_sortie, '<TOTPAIETTC>' || c_r_corp.totpaiettc || '</TOTPAIETTC>');
                    UTL_FILE.put_line(l_fic_sortie, '</TOTPAIE>');
                    UTL_FILE.put_line(l_fic_sortie, '<DATEPREL>' || to_char(c_r_corp.payment_due_date,'DDMMYYYY') || '</DATEPREL>');
                    UTL_FILE.put_line(l_fic_sortie, '<LOB><CODEEMET>' || c_r_corp.CODEEMET || '</CODEEMET>');
                    UTL_FILE.put_line(l_fic_sortie, '<CODEETAB>' || c_r_corp.CODEETAB || '</CODEETAB>');
                    UTL_FILE.put_line(l_fic_sortie, '<CLE3>' || c_r_corp.CLE3 || '</CLE3>');
                    UTL_FILE.put_line(l_fic_sortie, '<CLE2>' || c_r_corp.CLE2 || '</CLE2>');
                    UTL_FILE.put_line(l_fic_sortie, '<REFOP>' || c_r_corp.REFOP || '</REFOP>');
                    UTL_FILE.put_line(l_fic_sortie, '<CDOC>' || c_r_corp.CDOC || '</CDOC>');
                    UTL_FILE.put_line(l_fic_sortie, '<CLE1>' || c_r_corp.CLE1 || '</CLE1>');
                    UTL_FILE.put_line(l_fic_sortie, '<CNAT>' || c_r_corp.CNAT || '</CNAT>');
                    UTL_FILE.put_line(l_fic_sortie, '<CCB>' || c_r_corp.CCB || '</CCB>');
                    UTL_FILE.put_line(l_fic_sortie, '</LOB>');
                    UTL_FILE.put_line(l_fic_sortie, '</CORP>');
            END LOOP;
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_corp: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;                    

            END;

        RETURN RPE_RPE_C_COMMUN.g_succes;                
        END partie_corp;


    ------------------------------------------------------------------------------------
    --    Sous-fonction : Traitement partie coranexe FDE10800
    ------------------------------------------------------------------------------------ 

    function partie_coranexe(bill_ref_no IN NUMBER,
                             bill_ref_resets IN NUMBER
                            )
    RETURN NUMBER   
    IS    
        -- Curseur traitement partie coranexe
        CURSOR c_coranexe (v_bill_ref_no IN number,
                           v_bill_ref_resets IN number)
        IS
          select distinct
                 rfhf.bill_ref_no,
                 rfhf.bill_ref_resets,
                 rfhf.account_no,
                 cmf.bill_address1,
                 cmf.bill_address2,
                 cmf.bill_address3,
                 cmf.bill_zip,
                 cmf.bill_city,
                 la.address_1,
                 la.address_2,
                 la.address_3,
                 la.postal_code,
                 la.city,
                 cem.EXTERNAL_ID_TYPE,
                 cem.external_id
          from RPE_FAC_S_HEAD_FACT_NEW rfhf
          inner join CMF cmf on cmf.account_no = rfhf.account_no
          inner join SERVICE srv on srv.parent_account_no = rfhf.account_no
          inner join SERVICE_ADDRESS_ASSOC sad on sad.account_no = srv.parent_account_no
          inner join LOCAL_ADDRESS la on la.address_id = sad.address_id
          inner join CUSTOMER_ID_EQUIP_MAP cem on cem.SUBSCR_NO = srv.SUBSCR_NO
                                               and cem.EXTERNAL_ID_TYPE = srv.EMF_CONFIG_ID
          where rfhf.bill_ref_no = v_bill_ref_no
          and rfhf.bill_ref_resets = v_bill_ref_resets
          and (cem.EXTERNAL_ID_TYPE = 1
               or cem.EXTERNAL_ID_TYPE = 3
               or cem.EXTERNAL_ID_TYPE = 6)
          and (srv.SERVICE_INACTIVE_DT is null
               or srv.SERVICE_INACTIVE_DT >= rfhf.FROM_DATE);

          
            
        BEGIN
         RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_coranexe',RPE_RPE_COM_C_LOG.g_info); 
          BEGIN       
                UTL_FILE.put_line(l_fic_sortie, '<CORANEXE>');
                UTL_FILE.put_line(l_fic_sortie, '<ADRLIV>');
                FOR c_r_coranexe IN c_coranexe (bill_ref_no, bill_ref_resets) LOOP
                    UTL_FILE.put_line(l_fic_sortie, '<CMFADRCMP>' || c_r_coranexe.bill_address1 || ' ' ||
                                                                     c_r_coranexe.bill_address2 || ' ' ||
                                                                     c_r_coranexe.bill_address3 || ' ' ||
                                                                     c_r_coranexe.bill_zip || ' ' ||
                                                                     c_r_coranexe.bill_city ||
                                                    '</CMFADRCMP>');                 
                    UTL_FILE.put_line(l_fic_sortie, '<SERVADRCMP>' || c_r_coranexe.address_1 || ' '
                                                                   || c_r_coranexe.address_2 || ' '
                                                                   || c_r_coranexe.address_3 || ' '
                                                                   || c_r_coranexe.postal_code || ' '
                                                                   || c_r_coranexe.city ||                                                                  
                                                    '</SERVADRCMP>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR1>' || c_r_coranexe.address_1 || '</ADR1>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR2>' || c_r_coranexe.address_2 || '</ADR2>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR2></ADR2>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR3>' || c_r_coranexe.address_3 || '</ADR3>');
                    UTL_FILE.put_line(l_fic_sortie, '<ADR3></ADR3>');
                    UTL_FILE.put_line(l_fic_sortie, '<CP>' || c_r_coranexe.postal_code || '</CP>');
                    UTL_FILE.put_line(l_fic_sortie, '<VILLE>' || c_r_coranexe.city || '</VILLE>');
                    UTL_FILE.put_line(l_fic_sortie, '<NDI>' || c_r_coranexe.external_id || '</NDI>');
                END LOOP;
                    
                UTL_FILE.put_line(l_fic_sortie, '</ADRLIV>');
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_coranexe: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;                    

            END;

        RETURN RPE_RPE_C_COMMUN.g_succes;                
        END partie_coranexe;        
              
    ------------------------------------------------------------------------------------
    --    Sous-fonction : Traitement partie a echoir
    ------------------------------------------------------------------------------------ 
            
    function partie_a_echoir (bill_ref_no IN NUMBER,
                              bill_ref_resets IN NUMBER,
                              from_date IN DATE,
                              to_date IN DATE,
                              next_to_date IN DATE
                              )
    RETURN NUMBER
    IS   
       l_genre NUMBER(3):= 0;
       l_genre_special NUMBER(1);
    
        -- Curseur par facture sur la periode a echoir
      CURSOR c_rubr1_fact_echoir (v_bill_ref_no IN number,
                                  v_bill_ref_resets IN number)
      IS
      select bill_ref_no,
             bill_ref_resets,
             replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
             component_id_opt,
             RUBR,
             SSRUBR,
             CATEGORIE,
             SCATEGORIE,
             CATDET,
             Priority,
             duration,
             Duration_units,
             quantity,
             From_date,
             To_date,
			 sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, --FDE 10765
             TVA,
             sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
             sum(PX1) as PX1,
             sum(PX2) as PX2,
             sum(PX3) as PX3,
             STATUS_CAT_FACT,
			 END_DISCOUNT,
             Affichage -- FDE 10755
        from RPE_FAC_S_DET_FACT_NEW rfdf
       where rfdf.bill_ref_no = v_bill_ref_no
         and rfdf.bill_ref_resets = v_bill_ref_resets
         and rfdf.rubr = 1
         and rfdf.ssrubr = 1
         group by bill_ref_no,
             bill_ref_resets,
             replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
             Component_id_opt,
             RUBR,
             SSRUBR,
             CATEGORIE,
             SCATEGORIE,
             CATDET,
             Priority,
             duration,
             Duration_units,
             quantity,
             From_date,
             To_date,
             TVA,
             STATUS_CAT_FACT,
			 END_DISCOUNT,
             AFFICHAGE
       order by CATEGORIE, nvl(SCATEGORIE,0), priority, trunc(min(to_date_cycle)), min(component_id), component_id_opt;

        
        BEGIN
          RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_a_echoir',RPE_RPE_COM_C_LOG.g_info);            
          BEGIN
            FOR c_r1_echoir IN c_rubr1_fact_echoir (bill_ref_no, bill_ref_resets) LOOP

                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r1_echoir.RUBR || '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r1_echoir.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<CATEGORIE><genre>' || c_r1_echoir.CATEGORIE || '</genre>');

                l_genre_special :=0;

                IF c_r1_echoir.SCATEGORIE is not null then
                   UTL_FILE.put_line(l_fic_sortie, '<SCATEGORIE><genre>' || c_r1_echoir.SCATEGORIE || '</genre>');
                   if c_r1_echoir.CATDET=20 then l_genre_special:=1; --FDE10614 : identification remises pro ratees
                   l_genre:=10;
                   else l_genre := c_r1_echoir.CATDET;
                   end if;
                   --Debut FDE 10755
                   if (c_r1_echoir.CATEGORIE = 1) then
                      UTL_FILE.put_line(l_fic_sortie, '<AFFICHAGE>' || c_r1_echoir.AFFICHAGE || '</AFFICHAGE>');
                   end if;
                   -- FIN FDE 10755
                   l_enrgt := '<SCATDET><genre>' || l_genre || '</genre>';
               ELSE
                   if c_r1_echoir.CATDET=20 then l_genre_special:=1; --FDE10614 : identification remises pro ratees
                     l_genre:=10;
                   else l_genre := c_r1_echoir.CATDET;
                   end if;
                   --Debut FDE 10755
                   if (c_r1_echoir.CATEGORIE = 1) then
                      UTL_FILE.put_line(l_fic_sortie, '<AFFICHAGE>' || c_r1_echoir.AFFICHAGE || '</AFFICHAGE>');
                   end if;
                   -- FIN FDE 10755
                   l_enrgt := '<CATDET><genre>' || l_genre || '</genre>';
               END IF;

			   --Ajout de la date de fin de remise dans le libelle si disponible dans RPE_FAC_S_DET_FACT_NEW (FDE 10695)
			   IF c_r1_echoir.END_DISCOUNT is not null THEN
					l_libelle := c_r1_echoir.DESCRIPTION || ' (fin en ' || to_char(c_r1_echoir.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';
					RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
			   ELSE
					l_libelle := c_r1_echoir.DESCRIPTION;
			   END IF;

                CASE
                    when l_genre = 10 and l_genre_special=0 and c_r1_echoir.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echoir.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';
                    when l_genre = 10 and l_genre_special=0 and c_r1_echoir.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echoir.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';

                    when l_genre = 11
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';

                    when l_genre = 12
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';

                    when l_genre = 13 and c_r1_echoir.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echoir.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>';
                             
                    when l_genre = 13 and c_r1_echoir.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echoir.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>';

                    when l_genre = 14
                        then l_enrgt := l_enrgt || '<LIB>' || l_libelle || '</LIB>';

                    when l_genre = 15 and c_r1_echoir.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echoir.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';
                             
                    when l_genre = 15 and c_r1_echoir.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echoir.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';

                    when l_genre = 16
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<QTE>' || c_r1_echoir.QUANTITY || '</QTE>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';
                             
                    --FDE10614 : affichage des remises pro ratees
                    when l_genre=10 and l_genre_special=1
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echoir.duration || '</DAYS>'
                             || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echoir.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>';
                END CASE;

                IF l_genre = 14 then
                    if c_r1_echoir.PX1 is not null or c_r1_echoir.PX1 <> 0 then
                            l_enrgt := l_enrgt || '<PX1>' || c_r1_echoir.PX1 || '</PX1>';
                    end if;
                    if c_r1_echoir.PX2 is not null or c_r1_echoir.PX2 <> 0 then
                            l_enrgt := l_enrgt || '<PX2>' || c_r1_echoir.PX2 || '</PX2>';
                    end if;
                    if c_r1_echoir.PX3 is not null or c_r1_echoir.PX3 <> 0 then
                            l_enrgt := l_enrgt || '<PX3>' || c_r1_echoir.PX3 || '</PX3>';
                    end if;
                    if c_r1_echoir.DURATION_UNITS = 160 then
                            l_enrgt := l_enrgt || '<MONTHS>' || c_r1_echoir.duration || '</MONTHS>' || '<FROM>' || to_char(c_r1_echoir.from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(c_r1_echoir.to_date - 1,'DDMMYYYY') || '</TO>' || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>'; -- FDE 10765
                    end if;
                    --FDE10614 : prise en compte des remises pro ratees--
                    if c_r1_echoir.DURATION_UNITS = 140 then
                      l_enrgt := l_enrgt || '<DAYS>' || c_r1_echoir.duration || '</DAYS>' || '<PXHT>' || c_r1_echoir.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_echoir.PXTTC || '</PXTTC>'; -- FDE 10765
                    end if;
                END IF;

                IF c_r1_echoir.SCATEGORIE is not null then
                    l_enrgt := l_enrgt || '</SCATDET>';
                    UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                    UTL_FILE.put_line(l_fic_sortie, '</SCATEGORIE>');
                ELSE
                    l_enrgt := l_enrgt || '</CATDET>';
                    UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                END IF;
                    
                UTL_FILE.put_line(l_fic_sortie, '</CATEGORIE>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');   
            END LOOP;
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_a_echoir: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;                    

            END;

        RETURN RPE_RPE_C_COMMUN.g_succes;                
        END partie_a_echoir;
            
        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement partie echu
        ------------------------------------------------------------------------------------ 
 
        function partie_echu (bill_ref_no IN NUMBER,
                              bill_ref_resets IN NUMBER,
                              from_date IN DATE,
                              to_date IN DATE,
                              next_to_date IN DATE
                              )
        RETURN NUMBER
        IS
            l_genre NUMBER(3):= 0;
            l_genre_special NUMBER(1);
        
            -- Curseur par factures sur la periode a echu
            CURSOR c_rubr1_fact_echu (v_bill_ref_no IN number,
                                      v_bill_ref_resets IN number
                                      ) 
            IS
                select Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     duration,
                     Duration_units,
                     quantity,
                     From_date,
                     To_date,
                     sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, --FDE 10765
                     TVA,
                     sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                     sum(PX1) as PX1,
                     sum(PX2) as PX2,
                     sum(PX3) as PX3,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 1
                and rfdf.ssrubr = 2
                group by Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     duration,
                     Duration_units,
                     quantity,
                     From_date,
                     To_date,
                     TVA,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                order by rfdf.categorie, nvl(rfdf.scategorie,0), trunc(min(rfdf.to_date_cycle)), trunc(rfdf.from_date), trunc(rfdf.to_date) , rfdf.priority, min(rfdf.component_id), rfdf.component_id_opt;
       
        BEGIN
                      RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_echu',RPE_RPE_COM_C_LOG.g_info);
          -- Debut traitement 
          BEGIN
            FOR c_r1_echu IN c_rubr1_fact_echu (bill_ref_no, bill_ref_resets) LOOP

                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r1_echu.RUBR || '</genre>' || '<FROM>' || to_char (from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char (to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r1_echu.SSRUBR || '</genre>' || '<TO>' || to_char (to_date,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<CATEGORIE><genre>' || c_r1_echu.CATEGORIE || '</genre>');
                l_genre_special :=0;
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Catdet oÃ¹ es tu number1?' || l_genre || l_genre_special,RPE_RPE_COM_C_LOG.g_info);
                IF  c_r1_echu.SCATEGORIE is not null THEN
                       UTL_FILE.put_line(l_fic_sortie, '<SCATEGORIE><genre>' || c_r1_echu.SCATEGORIE || '</genre>');
                       if c_r1_echu.CATDET=20 then 
                          l_genre_special:=1; --FDE10614 : identification remises pro ratees
                          l_genre := 10;
                 RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Catdet oÃ¹ es tu number2?' || l_genre || l_genre_special,RPE_RPE_COM_C_LOG.g_info);      
                       else l_genre := c_r1_echu.CATDET;
                       end if;
                l_enrgt := '<SCATDET><genre>' || l_genre|| '</genre>';
                ELSE
                    if c_r1_echu.CATDET=20 then 
                       l_genre_special:=1; --FDE10614 : identification remises pro ratees
                       l_genre:=10;
                    else l_genre := c_r1_echu.CATDET;
                    end if;
                l_enrgt := '<CATDET><genre>' || l_genre || '</genre>';
                END IF;

                ------ Debut FDE 10695
                l_libelle := c_r1_echu.DESCRIPTION;

                --Ajout du PXU dans le libelle si disponible dans RPE_FAC_S_DET_FACT_NEW (FDE 10695)
                IF c_r1_echu.PXU is not null THEN
                   l_libelle := l_libelle || ' (' || replace(to_char((c_r1_echu.PXU)/100), '.', ',') || ' â‚¬ Mensuel)';
                   RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Ajout du PXU au libelle', RPE_RPE_COM_C_LOG.g_info);
                END IF;

                --Ajout de la date de fin de remise dans le libelle si disponible dans RPE_FAC_S_DET_FACT_NEW (FDE 10695)
                IF c_r1_echu.END_DISCOUNT is not null THEN
                   l_libelle := l_libelle || ' (fin en ' || to_char(c_r1_echu.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';
                   RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                END IF;
                ------ Fin FDE 10695

                CASE
                    when l_genre = 10 and l_genre_special=0 and c_r1_echu.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echu.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                                    
                    when l_genre = 10 and l_genre_special=0 and c_r1_echu.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echu.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                             
                    when l_genre = 11
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                                    
                    when l_genre = 12
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                                    
                    when l_genre = 13 and c_r1_echu.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echu.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>';
                                    
                    when l_genre = 13 and c_r1_echu.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echu.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>';
                             
                    when l_genre = 14
                        then l_enrgt := l_enrgt || '<LIB>' || l_libelle || '</LIB>';

                    when l_genre = 15 and c_r1_echu.DURATION_UNITS = 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<MONTHS>' || c_r1_echu.duration || '</MONTHS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                             
                    when l_genre = 15 and c_r1_echu.DURATION_UNITS <> 160
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echu.duration || '</DAYS>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';

                    when l_genre = 16
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>'
                             || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>'
                             || '<QTE>' || c_r1_echu.QUANTITY || '</QTE>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                             --FDE10614 : affichage des remises pro ratees
                                     
                    when l_genre=10 and l_genre_special=1
                        then l_enrgt := l_enrgt
                             || '<LIB>' || l_libelle || '</LIB>'
                             || '<DAYS>' || c_r1_echu.duration || '</DAYS>'
                             || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' --FDE 10765
                             || '<TVA>' || c_r1_echu.TVA || '</TVA>'
                             || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                END CASE;

                IF l_genre = 14 then
                    if c_r1_echu.PX1 is not null or c_r1_echu.PX1 <> 0 then
                       l_enrgt := l_enrgt || '<PX1>' || c_r1_echu.PX1 || '</PX1>';
                    end if;
                    if c_r1_echu.PX2 is not null or c_r1_echu.PX2 <> 0 then
                       l_enrgt := l_enrgt || '<PX2>' || c_r1_echu.PX2 || '</PX2>';
                    end if;
                    if c_r1_echu.PX3 is not null or c_r1_echu.PX3 <> 0 then
                       l_enrgt := l_enrgt || '<PX3>' || c_r1_echu.PX3 || '</PX3>';
                    end if;
                    if c_r1_echu.DURATION_UNITS = 160 then
                       l_enrgt := l_enrgt || '<MONTHS>' || c_r1_echu.duration || '</MONTHS>' || '<FROM>' || to_char(c_r1_echu.from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(c_r1_echu.to_date - 1,'DDMMYYYY') || '</TO>' || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                    end if;
                    if c_r1_echu.DURATION_UNITS =140 then --FDE10614 : identification remises multi tva pro ratees
                       l_enrgt := l_enrgt || '<DAYS>' || c_r1_echu.duration || '</DAYS>' || '<PXHT>' || c_r1_echu.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_echu.PXTTC || '</PXTTC>';
                    end if;
                END IF;

                IF c_r1_echu.SCATEGORIE is not null THEN
                   l_enrgt := l_enrgt || '</SCATDET>';
                   UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                   UTL_FILE.put_line(l_fic_sortie, '</SCATEGORIE>');
                ELSE
                   l_enrgt := l_enrgt || '</CATDET>';
                   UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                END IF;
                
                UTL_FILE.put_line(l_fic_sortie, '</CATEGORIE>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');
            END LOOP;
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_echu: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;

          END;            
            RETURN RPE_RPE_C_COMMUN.g_succes;
            
        END partie_echu;

      
        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement rubrique 2
        ------------------------------------------------------------------------------------ 
 
        function partie_RUBR2 (bill_ref_no IN NUMBER,
                              bill_ref_resets IN NUMBER,
                              from_date IN DATE,
                              to_date IN DATE,
                              next_to_date IN DATE
                              )
        RETURN NUMBER
        IS  
        
            -- Curseur rubrique 2      
        CURSOR c_rubr2 (v_bill_ref_no IN number,
                        v_bill_ref_resets IN number
                        )
        IS
                select Bill_ref_no,
                       Bill_ref_resets,
                       replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                       Component_id_opt,
                       RUBR,
                       SSRUBR,
                       CATEGORIE,
                       SCATEGORIE,
                       CATDET,
                       Priority,
                       duration,
                       Duration_units,
                       quantity,
                       From_date,
                       To_date,
                       sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, 
                       TVA,
                       sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                       sum(PX1) as PX1,
                       sum(PX2) as PX2,
                       sum(PX3) as PX3,
                       STATUS_CAT_FACT,
                       PXU,
                       END_DISCOUNT,
                       TYPE_CHARGE
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 2
                /* and rfdf.ssrubr = 2 */
                group by Bill_ref_no,
                         Bill_ref_resets,
                         replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                         Component_id_opt,
                         RUBR,
                         SSRUBR,
                         CATEGORIE,
                         SCATEGORIE,
                         CATDET,
                         Priority,
                         duration,
                         Duration_units,
                         quantity,
                         From_date,
                         To_date,
                         TVA,
                         STATUS_CAT_FACT,
                         PXU,
                         END_DISCOUNT,
                         TYPE_CHARGE
                order by rfdf.categorie,
                         nvl(rfdf.scategorie,0),
                         trunc(min(rfdf.to_date_cycle)),
                         trunc(rfdf.from_date),
                         trunc(rfdf.to_date),
                         rfdf.priority,
                         min(rfdf.component_id),
                         rfdf.component_id_opt;
                         
        BEGIN             
            --FDE 10799 rubrique 2
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Restitution rubrique 2', RPE_RPE_COM_C_LOG.g_info);
          BEGIN   
            FOR c_r_rubr2 IN c_rubr2 (bill_ref_no, bill_ref_resets) LOOP               
                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r_rubr2.rubr|| '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r_rubr2.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<ANXDET><genre>' || c_r_rubr2.CATDET || '</genre>');
                CASE
                    when nvl(c_r_rubr2.CATDET,0)=40 
                        then
                            l_enrgt := '<LIB>Comprises dans votre (vos) forfait(s)</LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr2.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr2.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
               
                    when nvl(c_r_rubr2.CATDET,0)=41
                        then
                            l_enrgt := '<LIB>En depassement et/ou hors forfait </LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr2.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr2.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr2.CATDET,0)=42 or nvl(c_r_rubr2.CATDET,0)=51
                        then
                            if c_r_rubr2.END_DISCOUNT is not null THEN
                               l_libelle := c_r_rubr2.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr2.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 2 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr2.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>';
                            if c_r_rubr2.type_charge = 4 then
                               l_enrgt := l_enrgt || '<TVA>' || c_r_rubr2.TVA || '</TVA>';
                            end if;
                            l_enrgt := l_enrgt|| '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
                        
                        
                    when nvl(c_r_rubr2.CATDET,0)=50 or nvl(c_r_rubr2.CATDET,0)=52
                        then
                            if c_r_rubr2.END_DISCOUNT is not null THEN
                               l_libelle := c_r_rubr2.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr2.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 2 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr2.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr2.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr2.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
                        
                    when nvl(c_r_rubr2.CATDET,0)=53
                        then
                            if c_r_rubr2.END_DISCOUNT is not null THEN
                               l_libelle := c_r_rubr2.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr2.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 2 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr2.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr2.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
                        
                    when nvl(c_r_rubr2.CATDET,0)=61 or nvl(c_r_rubr2.CATDET,0)=62 or nvl(c_r_rubr2.CATDET,0)=63 or nvl(c_r_rubr2.CATDET,0)=64 or nvl(c_r_rubr2.CATDET,0)=71 or nvl(c_r_rubr2.CATDET,0)=72 or nvl(c_r_rubr2.CATDET,0)=73 or nvl(c_r_rubr2.CATDET,0)=76 or nvl(c_r_rubr2.CATDET,0)=78
                        then
                            l_enrgt := '<FROM>' ||  to_char(c_r_rubr2.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr2.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr2.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr2.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr2.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr2.PXTTC || '</PXTTC>';
                    else null;
                END CASE;
                
                UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                UTL_FILE.put_line(l_fic_sortie, '</ANXDET>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');                
            END LOOP;
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_RUBR2: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;
          END;
            
            RETURN RPE_RPE_C_COMMUN.g_succes;  
        END partie_RUBR2;

        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement rubrique 3
        ------------------------------------------------------------------------------------ 
 
        function partie_RUBR3 (bill_ref_no IN NUMBER,
                               bill_ref_resets IN NUMBER,
                               from_date IN DATE,
                               to_date IN DATE,
                               next_to_date IN DATE
                               )
        RETURN NUMBER
        IS  
        
            --Curseur rubrique 3
        CURSOR c_rubr3 (v_bill_ref_no IN number,
                        v_bill_ref_resets IN number
                        ) 
        IS
                select Bill_ref_no,
                       Bill_ref_resets,
                       replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                       Component_id_opt,
                       RUBR,
                       SSRUBR,
                       CATEGORIE,
                       SCATEGORIE,
                       CATDET,
                       Priority,
                       duration,
                       Duration_units,
                       quantity,
                       From_date,
                       To_date,
                       sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, 
                       TVA,
                       sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                       sum(PX1) as PX1,
                       sum(PX2) as PX2,
                       sum(PX3) as PX3,
                       STATUS_CAT_FACT,
                       PXU,
                       END_DISCOUNT,
                       TYPE_CHARGE
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 3
                /* and rfdf.ssrubr = 2 */
                group by Bill_ref_no,
                         Bill_ref_resets,
                         replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                         Component_id_opt,
                         RUBR,
                         SSRUBR,
                         CATEGORIE,
                         SCATEGORIE,
                         CATDET,
                         Priority,
                         duration,
                         Duration_units,
                         quantity,
                         From_date,
                         To_date,
                         TVA,
                         STATUS_CAT_FACT,
                         PXU,
                         END_DISCOUNT,
                         TYPE_CHARGE
                order by rfdf.categorie,
                         nvl(rfdf.scategorie,0),
                         trunc(min(rfdf.to_date_cycle)),
                         trunc(rfdf.from_date), trunc(rfdf.to_date),
                         rfdf.priority, min(rfdf.component_id),
                         rfdf.component_id_opt;
                         
        BEGIN
            --FDE 10799 rubrique 3    
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Restitution rubrique 3', RPE_RPE_COM_C_LOG.g_info);    
          BEGIN 
            FOR c_r_rubr3 IN c_rubr3 (bill_ref_no, bill_ref_resets) LOOP
                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r_rubr3.rubr|| '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r_rubr3.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<ANXDET><genre>' || c_r_rubr3.CATDET || '</genre>');
                CASE
                    when nvl(c_r_rubr3.CATDET,0)=40
                        then
                            l_enrgt := '<LIB>Comprises dans votre (vos) forfait(s)</LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr3.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr3.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';
               
                    when nvl(c_r_rubr3.CATDET,0)=41
                        then
                            l_enrgt := '<LIB>En depassement et/ou hors forfait </LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr3.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr3.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr3.CATDET,0)=42 or nvl(c_r_rubr3.CATDET,0)=51
                        then                  
                            if c_r_rubr3.END_DISCOUNT is not null then
                               l_libelle := c_r_rubr3.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr3.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 3 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr3.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>';
                            if c_r_rubr3.type_charge = 4 then
                               l_enrgt := l_enrgt || '<TVA>' || c_r_rubr3.TVA || '</TVA>';
                            end if;
                            l_enrgt := l_enrgt|| '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';     
             
                    when nvl(c_r_rubr3.CATDET,0)=50 or nvl(c_r_rubr3.CATDET,0)=52
                        then
                            if c_r_rubr3.END_DISCOUNT is not null then
                               l_libelle := c_r_rubr3.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr3.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 3 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr3.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr3.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr3.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr3.CATDET,0)=53
                        then
                            if c_r_rubr3.END_DISCOUNT is not null then
                               l_libelle := c_r_rubr3.DESCRIPTION || ' (fin en ' || to_char(c_r_rubr3.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';     
                               RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Rubrique 3 : Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                            else
                               l_libelle := c_r_rubr3.DESCRIPTION;
                            end if;
                            l_enrgt :=  '<LIB>' || l_libelle || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr3.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr3.CATDET,0)=61 or nvl(c_r_rubr3.CATDET,0)=62 or nvl(c_r_rubr3.CATDET,0)=63 or nvl(c_r_rubr3.CATDET,0)=64 or nvl(c_r_rubr3.CATDET,0)=71 or nvl(c_r_rubr3.CATDET,0)=72 or nvl(c_r_rubr3.CATDET,0)=73 or nvl(c_r_rubr3.CATDET,0)=76 or nvl(c_r_rubr3.CATDET,0)=78
                        then
                            l_enrgt := '<FROM>' ||  to_char(c_r_rubr3.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr3.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr3.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr3.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr3.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr3.PXTTC || '</PXTTC>';
                    else null;
                END CASE;              
                UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                UTL_FILE.put_line(l_fic_sortie, '</ANXDET>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');             
            END LOOP;
            
            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_RUBR3: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;

          END;
            
            RETURN RPE_RPE_C_COMMUN.g_succes;  
        END partie_RUBR3;

        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement rubrique 4
        ------------------------------------------------------------------------------------ 
 
        function partie_RUBR4 (bill_ref_no IN NUMBER,
                               bill_ref_resets IN NUMBER,
                               from_date IN DATE,
                               to_date IN DATE,
                               next_to_date IN DATE
                               )
        RETURN NUMBER
        IS  
        
        --Curseur rubrique 4
        CURSOR c_rubr4 (v_bill_ref_no IN number,
                        v_bill_ref_resets IN number)
        IS
                select Bill_ref_no,
                       Bill_ref_resets,
                       replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                       Component_id_opt,
                       RUBR,
                        SSRUBR,
                        CATEGORIE,
                        SCATEGORIE,
                        CATDET,
                        Priority,
                        duration,
                        Duration_units,
                        quantity,
                        From_date,
                        To_date,
                        sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, 
                        TVA,
                        sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                        sum(PX1) as PX1,
                        sum(PX2) as PX2,
                        sum(PX3) as PX3,
                        STATUS_CAT_FACT,
                        PXU,
                        END_DISCOUNT,
                        TYPE_CHARGE
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 4
                /* and rfdf.ssrubr = 2 */
                group by Bill_ref_no,
                         Bill_ref_resets,
                         replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                         Component_id_opt,
                         RUBR,
                         SSRUBR,
                         CATEGORIE,
                         SCATEGORIE,
                         CATDET,
                         Priority,
                         duration,
                         Duration_units,
                         quantity,
                         From_date,
                         To_date,
                         TVA,
                         STATUS_CAT_FACT,
                         PXU,
                         END_DISCOUNT,
                         TYPE_CHARGE
                order by rfdf.categorie,
                         nvl(rfdf.scategorie,0),
                         trunc(min(rfdf.to_date_cycle)),
                         trunc(rfdf.from_date), trunc(rfdf.to_date),
                         rfdf.priority,
                         min(rfdf.component_id),
                         rfdf.component_id_opt;
        BEGIN
            --FDE 10799 rubrique 4
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Restitution rubrique 4', RPE_RPE_COM_C_LOG.g_info);
          BEGIN 
            FOR c_r_rubr4 IN c_rubr4 (bill_ref_no, bill_ref_resets) LOOP
                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r_rubr4.rubr|| '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r_rubr4.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<ANXDET><genre>' || c_r_rubr4.CATDET || '</genre>');
                CASE
                    when nvl(c_r_rubr4.CATDET,0)=40
                        then
                            l_enrgt := '<LIB>Comprises dans votre (vos) forfait(s)</LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr4.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr4.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
               
                    when nvl(c_r_rubr4.CATDET,0)=41
                        then
                            l_enrgt := '<LIB>En depassement et/ou hors forfait </LIB>'
                            ||'<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<DUR>' || c_r_rubr4.duration || '</DUR>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr4.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr4.CATDET,0)=42 or nvl(c_r_rubr4.CATDET,0)=51
                        then
                            l_enrgt :=  '<LIB>' || c_r_rubr4.DESCRIPTION || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>';
                            if c_r_rubr4.type_charge = 4 then
                               l_enrgt := l_enrgt || '<TVA>' || c_r_rubr4.TVA || '</TVA>';
                            end if;
                            l_enrgt := l_enrgt|| '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr4.CATDET,0)=50 or nvl(c_r_rubr4.CATDET,0)=52
                        then
                            l_enrgt := '<LIB>' || c_r_rubr4.DESCRIPTION || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr4.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr4.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
                    
                    when nvl(c_r_rubr4.CATDET,0)=53
                        then
                            l_enrgt := '<LIB>' || c_r_rubr4.DESCRIPTION || '</LIB>'
                            || '<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr4.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
                    
                   when nvl(c_r_rubr4.CATDET,0)=61 or nvl(c_r_rubr4.CATDET,0)=62 or nvl(c_r_rubr4.CATDET,0)=63 or nvl(c_r_rubr4.CATDET,0)=64 or nvl(c_r_rubr4.CATDET,0)=71 or nvl(c_r_rubr4.CATDET,0)=72 or nvl(c_r_rubr4.CATDET,0)=73 or nvl(c_r_rubr4.CATDET,0)=76 or nvl(c_r_rubr4.CATDET,0)=78
                        then
                            l_enrgt := '<FROM>' ||  to_char(c_r_rubr4.from_date,'DDMMYYYY') || '</FROM>'
                            || '<TO>' || to_char(c_r_rubr4.to_date,'DDMMYYYY') || '</TO>'
                            || '<QTE>' || c_r_rubr4.quantity || '</QTE>'
                            || '<PXHT>' || c_r_rubr4.PXHT || '</PXHT>'
                            || '<TVA>' || c_r_rubr4.TVA || '</TVA>'
                            || '<PXTTC>' || c_r_rubr4.PXTTC || '</PXTTC>';
                    else null;
                END CASE;            
                UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                UTL_FILE.put_line(l_fic_sortie, '</ANXDET>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');               
            END LOOP;

            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_RUBR4: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;
          END;
            
            RETURN RPE_RPE_C_COMMUN.g_succes;  
        END partie_RUBR4;
        
                ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement rubrique 6 Depot de garantie FDE10800
        ------------------------------------------------------------------------------------ 
 
        function partie_RUBR6 (bill_ref_no IN NUMBER,
                               bill_ref_resets IN NUMBER,
                               from_date IN DATE,
                               to_date IN DATE,
                               next_to_date IN DATE
                               )
        RETURN NUMBER
        IS  
        
            --Curseur rubrique 6
            CURSOR c_rubr6 (v_bill_ref_no IN number,
                            v_bill_ref_resets IN number)
            IS
                    select Bill_ref_no,
                           Bill_ref_resets,
                           replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                           Component_id_opt,
                           RUBR,
                           SSRUBR,
                           CATEGORIE,
                           SCATEGORIE,
                           CATDET,
                           Priority,
                           duration,
                           Duration_units,
                           quantity,
                           From_date,
                           To_date,
                           sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, 
                           TVA,
                           sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                           STATUS_CAT_FACT,
                           PXU,
                           END_DISCOUNT,
                           TYPE_CHARGE
                    from RPE_FAC_S_DET_FACT_NEW rfdf
                    where rfdf.bill_ref_no = v_bill_ref_no
                    and rfdf.bill_ref_resets = v_bill_ref_resets
                    and rfdf.rubr = 6
                    and rfdf.catdet = 53
                    group by 
                    bill_ref_no,
                    Bill_ref_resets,
                           replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') ,
                           Component_id_opt,
                           RUBR,
                           SSRUBR,
                           CATEGORIE,
                           SCATEGORIE,
                           CATDET,
                           Priority,
                           duration,
                           Duration_units,
                           quantity,
                           From_date,
                           To_date,
                           TVA,
                           STATUS_CAT_FACT,
                           PXU,
                           END_DISCOUNT,
                           TYPE_CHARGE;
                
                
        BEGIN
          RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Restitution rubrique 6',RPE_RPE_COM_C_LOG.g_info); 
          BEGIN
            FOR c_r_rubr6 IN c_rubr6 (bill_ref_no, bill_ref_resets) LOOP
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Ligne curseur 6',RPE_RPE_COM_C_LOG.g_info);
                UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r_rubr6.RUBR|| '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r_rubr6.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                UTL_FILE.put_line(l_fic_sortie, '<ANXDET><genre>' || c_r_rubr6.CATDET || '</genre>');
                UTL_FILE.put_line(l_fic_sortie, '<LIB>' || c_r_rubr6.description || '</LIB>');
                UTL_FILE.put_line(l_fic_sortie, '<FROM>' || to_char(c_r_rubr6.from_date,'DDMMYYYY') || '</FROM>');
                UTL_FILE.put_line(l_fic_sortie, '<TO>' || to_char(c_r_rubr6.to_date,'DDMMYYYY') || '</TO>');                
                UTL_FILE.put_line(l_fic_sortie, '<PXHT>' || c_r_rubr6.PXHT || '</PXHT>');
                UTL_FILE.put_line(l_fic_sortie, '<TVA>' || c_r_rubr6.TVA || '</TVA>');
                UTL_FILE.put_line(l_fic_sortie, '<PXTTC>' || c_r_rubr6.PXTTC || '</PXTTC>');                
                UTL_FILE.put_line(l_fic_sortie, '</ANXDET>');
                UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                UTL_FILE.put_line(l_fic_sortie, '</RUBR>');               
            END LOOP;

            EXCEPTION
                WHEN OTHERS THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_RUBR6: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
                RETURN RPE_RPE_C_COMMUN.g_echec;
          END;
            
            RETURN RPE_RPE_C_COMMUN.g_succes;  
        END partie_RUBR6;
                
        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement partie FCT (rubrique 7)
        ----------------------------------------------------------------------------------

        function partie_FCT (bill_ref_no IN NUMBER,
                             bill_ref_resets IN NUMBER,
                             from_date IN DATE,
                             to_date IN DATE,
                             next_to_date IN DATE
                            )
        RETURN NUMBER
        IS
            l_genre NUMBER(3):= 0;
            l_genre_special NUMBER(1);
        
            -- Curseur par factures sur la partie FCT (rubrique 7)
            CURSOR c_rubr1_fact_fct (v_bill_ref_no IN number,
                                     v_bill_ref_resets IN number
                                     ) 
            IS
                select Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     sum(duration) as duration,
                     Duration_units,
                     quantity,
                     case 
                        when substr(component_id,0,1) <> '4' then From_date
                        else null
                     end as From_date,                            
                     case 
                        when substr(component_id,0,1) <> '4' then To_date
                        else null
                     end as To_date,                             
                     sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, --FDE 10765
                     case 
                        when substr(component_id,0,1) <> '4' then TVA
                        else null
                     end as TVA,                            
                     sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                     sum(PX1) as PX1,
                     sum(PX2) as PX2,
                     sum(PX3) as PX3,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 7
                and rfdf.ssrubr in (23,24,5)
                group by Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     Duration_units,
                     quantity,
                     rfdf.COMPONENT_ID,
                     case 
                        when substr(component_id,0,1) <> '4' then From_date
                        else null
                     end,
                     case 
                        when substr(component_id,0,1) <> '4' then To_date
                        else null
                     end,
                     case 
                        when substr(component_id,0,1) <> '4' then TVA
                        else null
                     end,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                order by rfdf.priority, rfdf.COMPONENT_ID,
                         case 
                            when substr(component_id,0,1) <> '4' then From_date
                            else null
                         end,
                         case 
                            when substr(component_id,0,1) <> '4' then To_date
                            else null
                         end,
                         case 
                            when substr(component_id,0,1) <> '4' then TVA
                            else null
                         end,
                         rfdf.COMPONENT_ID_OPT;

            BEGIN
              RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_FCT',RPE_RPE_COM_C_LOG.g_info); 
              BEGIN

                FOR c_r1_fct IN c_rubr1_fact_fct (bill_ref_no, bill_ref_resets) LOOP
                
                    UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r1_fct.RUBR || '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                    UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r1_fct.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                    l_genre_special :=0;

                    IF c_r1_fct.SCATEGORIE is not null then
                        UTL_FILE.put_line(l_fic_sortie, '<SCATEGORIE><genre>' || c_r1_fct.SCATEGORIE || '</genre>');
                        if c_r1_fct.CATDET=20 then 
                           l_genre_special:=1; --FDE10614 : identification remises pro ratees
                           l_genre:=10;
                        else 
                           l_genre := c_r1_fct.CATDET;
                        end if;
                        l_enrgt := '<SCATDET><genre>' || l_genre || '</genre>';

                    ELSE
                        if c_r1_fct.CATDET=20 then
                           l_genre_special:=1; --FDE10614 : identification remises pro ratees
                           l_genre:=10;
                        else l_genre := c_r1_fct.CATDET;
                        end if;
                        l_enrgt := '<ANXDET><genre>' || l_genre || '</genre>';
                    END IF;

                    --Ajout de la date de fin de remise dans le libelle si disponible dans RPE_FAC_S_DET_FACT_NEW (FDE 10695)
                    IF c_r1_fct.END_DISCOUNT is not null then
                            l_libelle := c_r1_fct.DESCRIPTION || ' (fin en ' || to_char(c_r1_fct.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';
                            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                    ELSE
                            l_libelle := c_r1_fct.DESCRIPTION;
                    END IF;

                    CASE
                        when l_genre = 110  --RC product_line = 23
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXU>' || c_r1_fct.PXU || '</PXU>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 111  --Discount
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 112  --Ajustements
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<DAYS>' || c_r1_fct.duration || '</DAYS>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 113  --RC product_line = 24
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<PXU>' || c_r1_fct.PXU || '</PXU>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 114 and c_r1_fct.DURATION_UNITS = 160  --RC product_line = 25 (months)
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<MONTHS>' || c_r1_fct.duration || '</MONTHS>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXU>' || c_r1_fct.PXU || '</PXU>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 114 and c_r1_fct.DURATION_UNITS <> 160  --RC product_line = 25 (days)
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<DAYS>' || c_r1_fct.duration || '</DAYS>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXU>' || c_r1_fct.PXU || '</PXU>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 115  --NRC product_line = 23
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 116  --SVOD
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';


                        when l_genre = 117  --NRC product_line = 27
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 118  --Ticket ACTE
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 119  --NRC product_line = 29
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre=120   --Abonnements internet +
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre=121   --Achats internet +
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre=126   --Appels surtaxes factures Ã  la duree
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<DUR>' || c_r1_fct.DURATION || '</DUR>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre=127   --Appels surtaxes 118
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<DUR>' || c_r1_fct.DURATION || '</DUR>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre=128   ----Appels surtaxes factures Ã  l'appel
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_fct.QUANTITY || '</QTE>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<TVA>' || c_r1_fct.TVA || '</TVA>'
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';

                        when l_genre = 11  --Remise dynamique
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' --FDE 10765
                                 || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';
                        else null;
                    END CASE;

                    IF l_genre = 14 then
                        if c_r1_fct.PX1 is not null or c_r1_fct.PX1 <> 0 then
                           l_enrgt := l_enrgt || '<PX1>' || c_r1_fct.PX1 || '</PX1>';
                        end if;
                        if c_r1_fct.PX2 is not null or c_r1_fct.PX2 <> 0 then
                           l_enrgt := l_enrgt || '<PX2>' || c_r1_fct.PX2 || '</PX2>';
                        end if;
                        if c_r1_fct.PX3 is not null or c_r1_fct.PX3 <> 0 then
                           l_enrgt := l_enrgt || '<PX3>' || c_r1_fct.PX3 || '</PX3>';
                        end if;
                        if c_r1_fct.DURATION_UNITS = 160 then
                                  l_enrgt := l_enrgt || '<MONTHS>' || c_r1_fct.duration || '</MONTHS>' || '<FROM>' || to_char(c_r1_fct.from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(c_r1_fct.to_date - 1,'DDMMYYYY') || '</TO>' || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';
                        end if;
                        --FDE10614 : prise en compte des remises pro ratees--
                        if c_r1_fct.DURATION_UNITS = 140 then
                            l_enrgt := l_enrgt || '<DAYS>' || c_r1_fct.duration || '</DAYS>' || '<PXHT>' || c_r1_fct.PXHT || '</PXHT>' || '<PXTTC>' || c_r1_fct.PXTTC || '</PXTTC>';
                        end if;
                    END IF;

                    IF c_r1_fct.SCATEGORIE is not null then
                       l_enrgt := l_enrgt || '</SCATDET>';
                       UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                       UTL_FILE.put_line(l_fic_sortie, '</SCATEGORIE>');
                    ELSE
                        l_enrgt := l_enrgt || '</ANXDET>';
                        UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                    END IF;
                        UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                        UTL_FILE.put_line(l_fic_sortie, '</RUBR>');
                END LOOP; 

            EXCEPTION
            WHEN OTHERS THEN
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_FCT: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
            RETURN RPE_RPE_C_COMMUN.g_echec;
            END;

        RETURN RPE_RPE_C_COMMUN.g_succes;                
        END partie_FCT;

        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement partie PCT (rubrique 8)
        ----------------------------------------------------------------------------------

        function partie_PCT (bill_ref_no IN NUMBER,
                             bill_ref_resets IN NUMBER,
                             from_date IN DATE,
                             to_date IN DATE,
                             next_to_date IN DATE
                             )
        RETURN NUMBER
        IS
            l_genre NUMBER(3):= 0;
            l_genre_special NUMBER(1);
            
            CURSOR c_rubr1_fact_pct (v_bill_ref_no IN number,
                                     v_bill_ref_resets IN number
                                     ) 
            IS
                select Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     duration,
                     Duration_units,
                     quantity,
                     From_date,
                     To_date,
                     sum(nvl(PRIX_NET_HT, PXHT)) as PXHT, --FDE 10765
                     TVA,
                     sum(nvl(PRIX_NET_TTC,PXTTC)) as PXTTC,
                     sum(PX1) as PX1,
                     sum(PX2) as PX2,
                     sum(PX3) as PX3,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 8
                and rfdf.ssrubr in (25, 26)
                group by Bill_ref_no,
                     Bill_ref_resets,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;'),
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     duration,
                     Duration_units,
                     quantity,
                     From_date,
                     To_date,
                     TVA,
                     STATUS_CAT_FACT,
                     PXU,
                     END_DISCOUNT
                order by rfdf.categorie, nvl(rfdf.scategorie,0), trunc(min(rfdf.to_date_cycle)), trunc(rfdf.from_date), trunc(rfdf.to_date) , rfdf.priority, min(rfdf.component_id), rfdf.component_id_opt;

            BEGIN
              RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_PCT',RPE_RPE_COM_C_LOG.g_info); 
              BEGIN
              
                FOR c_r1_pct IN c_rubr1_fact_pct (bill_ref_no, bill_ref_resets) LOOP
                    UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r1_pct.RUBR || '</genre>' || '<FROM>' || to_char(from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(to_date-1,'DDMMYYYY') || '</TO>');
                    UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r1_pct.SSRUBR || '</genre>' || '<FROM>' || to_char(to_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(next_to_date-1,'DDMMYYYY') || '</TO>');
                    l_genre_special :=0;

                    IF c_r1_pct.SCATEGORIE is not null then
                        UTL_FILE.put_line(l_fic_sortie, '<SCATEGORIE><genre>' || c_r1_pct.SCATEGORIE || '</genre>');
                        if c_r1_pct.CATDET=20 then
                        l_genre_special:=1; --FDE10614 : identification remises pro ratees
                        l_genre:=10;
                        else l_genre := c_r1_pct.CATDET;
                        end if;
                        l_enrgt := '<SCATDET><genre>' || l_genre || '</genre>';
                    ELSE
                        if c_r1_pct.CATDET=20 then l_genre_special:=1; --FDE10614 : identification remises pro ratees
                        l_genre:=10;
                        else l_genre := c_r1_pct.CATDET;
                        end if;
                        l_enrgt := '<ANXDET><genre>' || l_genre || '</genre>';
                    END IF;

                    --Ajout de la date de fin de remise dans le libelle si disponible dans RPE_FAC_S_DET_FACT_NEW (FDE 10695)
                    IF c_r1_pct.END_DISCOUNT is not null then
                       l_libelle := c_r1_pct.DESCRIPTION || ' (fin en ' || to_char(c_r1_pct.END_DISCOUNT, 'FMMonth YYYY', 'nls_date_language=french') || ')';
                       RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Ajout de la date de fin de remise au libelle', RPE_RPE_COM_C_LOG.g_info);
                    ELSE
                       l_libelle := c_r1_pct.DESCRIPTION;
                    END IF;

                    CASE
                        when l_genre=122    --Abonnements internet +
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_pct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_pct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_pct.QUANTITY || '</QTE>'
                                 || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';

                        when l_genre = 123  --NRC product_line = 29
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_pct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_pct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_pct.QUANTITY || '</QTE>'
                                 || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';

                        when l_genre=124   --Achats internet +
                            then l_enrgt := l_enrgt
                                 || '<FROM>' || to_char(c_r1_pct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_pct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_pct.QUANTITY || '</QTE>'
                                 || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';

                        when l_genre = 125  --NRC product_line = 30
                            then l_enrgt := l_enrgt
                                 || '<LIB>' || l_libelle || '</LIB>'
                                 || '<FROM>' || to_char(c_r1_pct.from_date,'DDMMYYYY') || '</FROM>'
                                 || '<TO>' || to_char(c_r1_pct.to_date - 1,'DDMMYYYY') || '</TO>'
                                 || '<QTE>' || c_r1_pct.QUANTITY || '</QTE>'
                                 || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';
                        else null;
                    END CASE;

                        IF l_genre = 14 then
                            if c_r1_pct.PX1 is not null or c_r1_pct.PX1 <> 0 then
                               l_enrgt := l_enrgt || '<PX1>' || c_r1_pct.PX1 || '</PX1>';
                            end if;
                            if c_r1_pct.PX2 is not null or c_r1_pct.PX2 <> 0 then
                               l_enrgt := l_enrgt || '<PX2>' || c_r1_pct.PX2 || '</PX2>';
                            end if;
                            if c_r1_pct.PX3 is not null or c_r1_pct.PX3 <> 0 then
                               l_enrgt := l_enrgt || '<PX3>' || c_r1_pct.PX3 || '</PX3>';
                            end if;
                            if c_r1_pct.DURATION_UNITS = 160 then
                               l_enrgt := l_enrgt || '<MONTHS>' || c_r1_pct.duration || '</MONTHS>' || '<FROM>' || to_char(c_r1_pct.from_date,'DDMMYYYY') || '</FROM>' || '<TO>' || to_char(c_r1_pct.to_date - 1,'DDMMYYYY') || '</TO>' || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';
                            end if;
                            --FDE10614 : prise en compte des remises pro ratees--
                            if c_r1_pct.DURATION_UNITS = 140 then
                               l_enrgt := l_enrgt || '<DAYS>' || c_r1_pct.duration || '</DAYS>' || '<PXTTC>' || c_r1_pct.PXTTC || '</PXTTC>';
                            end if;
                        END IF;

                        IF c_r1_pct.SCATEGORIE is not null then
                            l_enrgt := l_enrgt || '</SCATDET>';
                            UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                            UTL_FILE.put_line(l_fic_sortie, '</SCATEGORIE>');
                        ELSE
                            l_enrgt := l_enrgt || '</ANXDET>';
                            UTL_FILE.put_line(l_fic_sortie, l_enrgt);
                        END IF;                      
                        UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                        UTL_FILE.put_line(l_fic_sortie, '</RUBR>');
                END LOOP;

              EXCEPTION
              WHEN OTHERS THEN
              RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_PCT: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
              RETURN RPE_RPE_C_COMMUN.g_echec;
            END;

        RETURN RPE_RPE_C_COMMUN.g_succes;                
        END partie_PCT;

        ----------------------------------------------------------------------------------
        --    Sous-fonction : Traitement rubrique 9
        ----------------------------------------------------------------------------------

        function partie_RUBR9 (bill_ref_no IN NUMBER,
                              bill_ref_resets IN NUMBER,
                              from_date IN DATE,
                              to_date IN DATE,
                              next_to_date IN DATE
                              )
        RETURN NUMBER
        IS
            l_genre NUMBER(3):= 0;
            l_genre_special NUMBER(1);
            
            CURSOR c_rubr9_fact (v_bill_ref_no IN number,
                                 v_bill_ref_resets IN number
                                 )
            IS
                select Bill_ref_no,
                     Bill_ref_resets,
                     Component_id,
                     replace(replace(replace(replace(replace(rfdf.description,'&','&amp;'),'<','&lt;'),'>','&gt;'),'"','&quot;'),'''','&apos;') as description,
                     Component_id_opt,
                     RUBR,
                     SSRUBR,
                     CATEGORIE,
                     SCATEGORIE,
                     CATDET,
                     Priority,
                     duration,
                     Duration_units,
                     quantity,
                     From_date,
                     To_date,
                     nvl(PRIX_NET_TTC,PXTTC) as PXHT, --FDE 10765, TVA 0 donc PXHT = PXTTC
                     TVA,
                     nvl(PRIX_NET_TTC,PXTTC) as PXTTC,
                     PX1,
                     PX2,
                     PX3,
                     STATUS_CAT_FACT
                from RPE_FAC_S_DET_FACT_NEW rfdf
                where rfdf.bill_ref_no = v_bill_ref_no
                and rfdf.bill_ref_resets = v_bill_ref_resets
                and rfdf.rubr = 9;
          
            BEGIN
              RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction partie_RUBR9',RPE_RPE_COM_C_LOG.g_info);
              -- Debut traitement  
              BEGIN
              
                FOR c_r9_fact IN c_rubr9_fact (bill_ref_no, bill_ref_resets) LOOP
                    UTL_FILE.put_line(l_fic_sortie, '<RUBR><genre>' || c_r9_fact.RUBR || '</genre>' || '<PX>' || c_r9_fact.PXTTC || '</PX>');
                    UTL_FILE.put_line(l_fic_sortie, '<SSRUBR><genre>' || c_r9_fact.SSRUBR || '</genre>' || '<PX>' || c_r9_fact.PXTTC || '</PX>');
                    UTL_FILE.put_line(l_fic_sortie, '<ANXDET><genre>' || c_r9_fact.CATDET || '</genre><LIB>' || c_r9_fact.description || '</LIB>' || '<FROM>' || to_char (to_date,'DDMMYYYY') || '</FROM>' || '<PXHT>' || c_r9_fact.PXHT || '</PXHT>' || '<TVA>0</TVA>' || '<PXTTC>' || c_r9_fact.PXTTC || '</PXTTC>' || '</ANXDET>'); --FDE 10765
                    UTL_FILE.put_line(l_fic_sortie, '</SSRUBR>');
                    UTL_FILE.put_line(l_fic_sortie, '</RUBR>');
                END LOOP;

              EXCEPTION
              WHEN OTHERS THEN
              RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,SQLERRM,'Exception dans la fonction partie_RUBR9: '||SQLERRM ,RPE_RPE_COM_C_LOG.g_warning);
              RETURN RPE_RPE_C_COMMUN.g_echec;
              END;

            RETURN RPE_RPE_C_COMMUN.g_succes;                
            END partie_RUBR9;

----------------------------------------------------------------------------------
--    Fonction globale : Lecture dossier et boucle d'ouverture de facture + extraction des rubriques
----------------------------------------------------------------------------------     
function extract_fac_rubr(p_rep_file_out IN VARCHAR2,
                          p_fact_type IN NUMBER,
                          p_idf_serveur IN NUMBER
                          )
return number
IS
ERREUR_PROC EXCEPTION;
CODE_RETOUR NUMBER;
 
    -- Curseur sur les factures par fichier

    CURSOR c_file_fact IS
      select bi.file_name, bi.bill_ref_no, bi.bill_ref_resets, rfhf.from_date, rfhf.to_date, rfhf.next_to_date
      from RPE_FAC_S_HEAD_FACT_NEW rfhf
      inner join bill_invoice bi ON bi.bill_ref_no = rfhf.bill_ref_no
      where rfhf.fact_type = p_fact_type
      and rfhf.serveur = p_idf_serveur
      and bi.format_status = 0
      and bi.format_error_code is null
      order by bi.file_name;
      
      
    -- Debut traitement fonction globale
    BEGIN
      BEGIN
        FOR c_f_f IN c_file_fact LOOP

            IF (l_file_name <> c_f_f.file_name and l_bill_ref_no = 0) then
                --Ouverture du fichier
                select 'SP' || c_f_f.file_name into l_nom_fichier from dual;
                l_fic_sortie := UTL_FILE.Fopen(p_rep_file_out, l_nom_fichier, 'W');
                UTL_FILE.put_line(l_fic_sortie, '<RAC>');
                l_file_name := c_f_f.file_name;
                l_fac_existe := 1;
            ELSE
                if (l_file_name <> c_f_f.file_name) then
                    --Fermeture du fichier
                    UTL_FILE.put_line(l_fic_sortie, '</RAC>');
                    UTL_FILE.Fclose(l_fic_sortie);
                    --Ouverture du fichier
                    select 'SP' || c_f_f.file_name into l_nom_fichier from dual;
                    l_fic_sortie := UTL_FILE.Fopen(p_rep_file_out, l_nom_fichier, 'W');
                    UTL_FILE.put_line(l_fic_sortie, '<RAC>');
                    l_file_name := c_f_f.file_name;
                    l_fac_existe := 1;
                end if;
            END IF;
            l_bill_ref_no := c_f_f.bill_ref_no;
            l_bill_ref_resets := c_f_f.bill_ref_resets;
            l_debut_fac := 0; 

            -- Initialisation de la facture FDE 10800    
            IF (l_debut_fac = 0) THEN
            --debut de facture
             UTL_FILE.put_line(l_fic_sortie, '<FAC>');
            END IF;            
            l_debut_fac := 1;
            l_fac_existe := 2;
            
            IF (partie_corp(c_f_f.bill_ref_no, c_f_f.bill_ref_resets) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_corp',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
                   
            IF (partie_coranexe(c_f_f.bill_ref_no, c_f_f.bill_ref_resets) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_coranexe',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;  
                        
            IF (partie_a_echoir(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_a_echoir',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;          
            
            IF (partie_echu(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_echu.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
            
            IF (partie_RUBR2(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_RUBR2.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
            
            IF (partie_RUBR3(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_RUBR3.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
            
            IF (partie_RUBR4(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_RUBR4.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
            
            IF (partie_RUBR6(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_RUBR6.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;

            IF (partie_FCT(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_FCT.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
                  
            IF (partie_PCT(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_PCT.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;           
            
            IF (partie_RUBR9(c_f_f.bill_ref_no, c_f_f.bill_ref_resets, c_f_f.from_date, c_f_f.to_date, c_f_f.next_to_date) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de la fonction partie_RUBR9.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;
            
            -- Fonction globale partie fin de facture
            IF (l_debut_fac = 1) then
                 UTL_FILE.put_line(l_fic_sortie, '</CORANEXE>'); 
                 UTL_FILE.put_line(l_fic_sortie, '</FAC>');
            END IF;

            END loop;
            

            --    Fin de l'ensemble des factures
            IF (l_fac_existe = 1) then 
                    UTL_FILE.put_line(l_fic_sortie, '<FAC>');
                    UTL_FILE.put_line(l_fic_sortie, '</FAC>');
                    UTL_FILE.put_line(l_fic_sortie, '</RAC>');
            ELSE 
                    if (l_fac_existe > 1) then --facture complete
                        UTL_FILE.put_line(l_fic_sortie, '</RAC>');
                    end if;
            END IF;  

        EXCEPTION
            WHEN UTL_FILE.invalid_path THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (invalid_path) : ' || p_rep_file_out || l_nom_fichier, RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.invalid_mode THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (invalid_mode)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.invalid_operation THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (invalid_operation)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.invalid_maxlinesize THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (invalid_maxlinesize)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.access_denied THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (access_denied)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.invalid_filehandle THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (invalid_filehandle)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.write_error THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'ERREUR sur fichier de sortie (write_error)', RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log, NULL, 'Fin de la fonction extract_fac_rubr.', RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN UTL_FILE.charsetmismatch THEN

                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR sur fichier de sortie (charsetmismatch)',RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de la fonction extract_fac_rubr.',RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
            WHEN OTHERS THEN
                --
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Generation du fichier - erreur Oracle : ' || SQLERRM, RPE_RPE_COM_C_LOG.g_erreur);
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de la fonction extract_fac_rubr.',RPE_RPE_COM_C_LOG.g_erreur);
                UTL_FILE.Fclose(l_fic_sortie);
                RETURN rpe_rpe_c_commun.g_echec;
        END;

        RETURN RPE_RPE_C_COMMUN.g_succes;
    END extract_fac_rubr;


  -----------------------------------------------------------------------
    -- Description:
    -- Permet de lancer l'ensemble des traitements
    -- du package
    -----------------------------------------------------------------------
    -- Parameters
    -- Input  : les procedures a  lancer
    -- Output :
    -----------------------------------------------------------------------
    PROCEDURE Initier_Traitement(p_rep_out              IN VARCHAR2,
                                 p_rep_out_file         IN VARCHAR2,
                                 p_nom_fic_log          IN VARCHAR2,
                                 p_idf_serveur          IN NUMBER,
                                 p_fact_type            IN NUMBER)
    IS
        ERREUR_PROC EXCEPTION;   --Exception dans la procedure
        nom_fichier VARCHAR2(255);
        code_retour NUMBER(1) := 0;
        date_suivi_run VARCHAR2(8);

    BEGIN
        RPE_RPE_COM_C_LOG.Ouvrir_Log(p_rep_out,p_nom_fic_log,'RPE_FAC_S_EXTRACT_RUBR_NEW',p_rec_log);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut procedure Initier_Traitement. Date-heure : ' ||TO_CHAR(sysdate,'YYYY/MM/DD HH24:MI:SS'),RPE_RPE_COM_C_LOG.g_info);

        code_retour := 0;

        -- Appel de la fonction de selection des clients a basculer en mensuel
            RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Debut de la fonction extract_fac_rubr',RPE_RPE_COM_C_LOG.g_info);
            -- Realise l extraction
            IF (extract_fac_rubr(p_rep_out, p_fact_type, p_idf_serveur) = RPE_RPE_C_COMMUN.g_echec) THEN
                RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'ERREUR lors de l appel a la fonction extract_fac_rubr.',RPE_RPE_COM_C_LOG.g_warning);
                code_retour := 1;
                RAISE ERREUR_PROC;
            END IF;

        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de fonction extract_fac_rubr',RPE_RPE_COM_C_LOG.g_info);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de la procedure Initier_Traitement Date-heure : ' ||TO_CHAR(sysdate,'YYYY/MM/DD HH24:MI:SS'),RPE_RPE_COM_C_LOG.g_info);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Code retour SQL : ' || code_retour,RPE_RPE_COM_C_LOG.g_info);
        RPE_RPE_COM_C_LOG.Fermer_Log(p_rec_log);

    EXCEPTION
    WHEN ERREUR_PROC THEN
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'1 - ERREUR lors de l appel a la procedure initier_traitement',RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de la procedure Initier_Traitement',RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Code retour SQL : ' || code_retour,RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Fermer_Log(p_rec_log);
    WHEN OTHERS THEN
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'2 - ERREUR lors de l appel a la procedure initier_traitement',RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Fin de la procedure Initier_Traitement',RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Ecrire_Log(p_rec_log,NULL,'Code retour SQL : ' || code_retour,RPE_RPE_COM_C_LOG.g_warning);
        RPE_RPE_COM_C_LOG.Fermer_Log(p_rec_log);
        RAISE; -- sort en erreur
    END Initier_Traitement;

END RPE_FAC_S_EXTRACT_RUBR_NEW;    

/

SHOW ERRORS

EXIT;